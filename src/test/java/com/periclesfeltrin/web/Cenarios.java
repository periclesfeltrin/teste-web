package com.periclesfeltrin.web;

import com.periclesfeltrin.web.base.BaseTest;
import com.periclesfeltrin.web.page.Add;
import com.periclesfeltrin.web.page.Home;
import com.periclesfeltrin.web.page.HomeV4;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Cenarios extends BaseTest {

    @Test(description = "Realiza cadastro de cliente com sucesso.")
    public void validarAddCustomer() {
        HomeV4 homeV4_page = new Home(driver).mudarVersaoV4();
        Add page_add = homeV4_page.adicionarCliente();
        String mensagem = page_add.preencherFormESalvar("Teste Sicredi","Teste",
                "Péricles", "51 9999-9999", "Av Assis Brasil, 3970",
                "Torre D", "Porto Alegre", "RS", "91000-000","Brasil",
                "Fixter", "200");

        Assert.assertEquals(mensagem,
                "Your data has been successfully stored into the database. Edit Customer or Go back to list");
    }


    @Test(description = "Realiza a exclusão de um registro.")
    public void validarExclusaoDeRegistro(){
        validarAddCustomer();

        Add page_add = new Add(driver);
        HomeV4 page_home = page_add.voltarHomeV4();
        String mensagem_modal = page_home.deletarTodaPesquisa("Teste Sicredi");
        String mensagem_alerta = page_home.confirmarExclusao();

        Assert.assertEquals("Are you sure that you want to delete this 1 item?", mensagem_modal);
        Assert.assertEquals(mensagem_alerta,"Your data has been successfully deleted from the database.");
    }
}
