package com.periclesfeltrin.web.page;

import org.apache.bcel.ExceptionConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomeV4 extends PageObject {

    public HomeV4(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using = "switch-version-select")
    protected WebElement selectVersion;

    @FindBy(how = How.XPATH, using = "//div[@class='container-fluid gc-container']")
    protected WebElement container;

    @FindBy(how = How.CSS, using = "#gcrud-search-form > div.header-tools > div.floatL.t5 > a > i")
    protected WebElement addCustomer;

    @FindBy(how = How.XPATH, using = "//a[@class='btn btn-default btn-outline-dark gc-refresh']")
    protected WebElement refresh;

    @FindBy(how = How.NAME, using = "customerName")
    protected WebElement customerName;

    @FindBy(how = How.XPATH, using = "//input[@class='select-all-none']")
    protected WebElement selectAllNone;

    @FindBy(how = How.XPATH, using = "//a[@class='btn btn-outline-dark delete-selected-button']//span[@class='text-danger'][contains(text(),'Delete')]")
    protected WebElement deleteAll;


    public void setSelectVersion(String selectVersion) {
        wait.until(ExpectedConditions.visibilityOf(this.selectVersion));
        selectByVisibleText(this.selectVersion, selectVersion);
    }

    public void verifyContainer(){
        wait.until(ExpectedConditions.visibilityOf(container));
    }

    public void clickAddCustomer() {
        wait.until(ExpectedConditions.elementToBeClickable(addCustomer)).click();
    }

    public void clickRefresh() {
        refresh.click();
        verifyContainer();
    }

    public void setCustomerName(String customerName) {
        wait.until(ExpectedConditions.visibilityOf(this.customerName)).sendKeys(customerName);
    }

    public void clickSelectAllNone(){
        selectAllNone.click();
    }

    public void clickDeleteAll(){
        wait.until(ExpectedConditions.elementToBeClickable(deleteAll)).click();
    }

    public void verifyModal(){
        String modal = "//div[@class='delete-multiple-confirmation modal fade in show']//div[@class='modal-dialog']";
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(modal)));
    }

    public String getAlertDelete(){
        String text = "//p[contains(@class, 'alert-delete-multiple')]";
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(text))).getText();
    }

    public void clickConfirmDelete(){
        String confirmDelete = "//button[@class='btn btn-danger delete-multiple-confirmation-button']";
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(confirmDelete))))).click();
    }

    public String getAlertSuccess(){
         String alert = "//p[contains(text(),'Your data has been successfully deleted')]";
         return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(alert))).getText();
    }

    public Add adicionarCliente(){
        clickRefresh();
        clickAddCustomer();
        return new Add(driver);
    }

    public String deletarTodaPesquisa(String cliente){
        setCustomerName(cliente);
        clickRefresh();
        clickSelectAllNone();
        clickDeleteAll();
        verifyModal();
        return getAlertDelete();
    }

    public String confirmarExclusao() {
        verifyModal();
        clickConfirmDelete();
        return getAlertSuccess();
    }
}