package com.periclesfeltrin.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Add extends PageObject {

    public Add(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using = "field-customerName")
    protected WebElement name;

    @FindBy(how = How.ID, using = "field-contactLastName")
    protected WebElement lastName;

    @FindBy(how = How.ID, using = "field-contactFirstName")
    protected WebElement contactFirstName;

    @FindBy(how = How.ID, using = "field-phone")
    protected WebElement phone;

    @FindBy(how = How.ID, using = "field-addressLine1")
    protected WebElement addressLine1;

    @FindBy(how = How.ID, using = "field-addressLine2")
    protected WebElement addressLine2;

    @FindBy(how = How.ID, using = "field-city")
    protected WebElement city;

    @FindBy(how = How.ID, using = "field-state")
    protected WebElement state;

    @FindBy(how = How.ID, using = "field-postalCode")
    protected WebElement postalCode;

    @FindBy(how = How.ID, using = "field-country")
    protected WebElement country;

    @FindBy(how = How.XPATH, using = "//a[@class='chosen-single chosen-default']")
    protected WebElement fromEmployeer;

    @FindBy(how = How.XPATH, using = "//div[@class='chosen-search']//input")
    protected WebElement findFromEmployeer;

    @FindBy(how = How.ID, using = "field-creditLimit")
    protected WebElement creditLimit;

    @FindBy(how = How.ID, using = "form-button-save")
    protected WebElement save;

    @FindBy(how = How.ID, using = "save-and-go-back-button")
    protected WebElement saveAndGoBack;

    @FindBy(how = How.ID, using = "report-success")
    protected WebElement reportSuccess;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Go back to list')]")
    protected WebElement goBackToList;


    public void setName(String name) {
        wait.until(ExpectedConditions.visibilityOf(this.name));
        this.name.sendKeys(name);
    }

    public void setLastName(String lastName) {
        this.lastName.sendKeys(lastName);
    }

    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName.sendKeys(contactFirstName);
    }

    public void setPhone(String phone) {
        this.phone.sendKeys(phone);
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1.sendKeys(addressLine1);
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2.sendKeys(addressLine2);
    }

    public void setCity(String city) {
        this.city.sendKeys(city);
    }

    public void setState(String state) {
        this.state.sendKeys(state);
    }

    public void setPostalCode(String postalCode) {
        this.postalCode.sendKeys(postalCode);
    }

    public void setCountry(String country) {
        this.country.sendKeys(country);
    }

    public void setFromEmployeer(String fromEmployeer) {
        this.fromEmployeer.click();
        findFromEmployeer.sendKeys(fromEmployeer);
        findFromEmployeer.sendKeys(Keys.ENTER);
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit.sendKeys(creditLimit);
    }

    public void clickSave() {
        save.click();
    }

    public void clickSaveAndGoBack() {
        saveAndGoBack.click();
    }

    public String getReportSuccess(){
        return wait.until(ExpectedConditions.visibilityOf(reportSuccess)).getText();
    }

    public void clickGoBackToList(){
        wait.until(ExpectedConditions.visibilityOf(goBackToList));
        goBackToList.click();
    }

    public String preencherFormESalvar(
            String nome, String sobrenome, String nomeContato, String telefone,
            String endereco1, String endereco2, String cidade, String estado, String cep, String pais,
            String doEmpregador, String creditoLimite
    ){
        setName(nome);
        setLastName(sobrenome);
        setContactFirstName(nomeContato);
        setPhone(telefone);
        setAddressLine1(endereco1);
        setAddressLine2(endereco2);
        setCity(cidade);
        setState(estado);
        setPostalCode(cep);
        setCountry(pais);
        setFromEmployeer(doEmpregador);
        setCreditLimit(creditoLimite);

        clickSave();

        return getReportSuccess();
    }

    public HomeV4 voltarHomeV4(){
        clickGoBackToList();
        return new HomeV4(driver);
    }
}
