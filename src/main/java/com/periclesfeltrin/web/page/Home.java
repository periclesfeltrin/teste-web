package com.periclesfeltrin.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Home extends PageObject {

    public Home(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using = "switch-version-select")
    protected WebElement selectVersion;

    public void setSelectVersion(String selectVersion) {
        wait.until(ExpectedConditions.visibilityOf(this.selectVersion));
        selectByVisibleText(this.selectVersion, selectVersion);
    }

    public HomeV4 mudarVersaoV4(){
        setSelectVersion("Bootstrap V4 Theme");
        return new HomeV4(driver);
    }
}